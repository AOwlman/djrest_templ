Django with REST framework cookiecutter template
============================

Шаблон Django проекта от owlman (owlman.net)

Использование
-------------

Разверните новое виртуальное окружение:

.. code-block:: shell

    python3 -m venv --system-site-packages env

С помощью `cookiecutter` создайте структуру проекта:

.. code-block:: shell

    pip install cookiecutter
    cookiecutter https://AOwlman@bitbucket.org/AOwlman/djrest_templ.git

В ходе создания заполните поля:

- **env_name**: Окружение для нового проекта. То в котором вы разворачиваете проект. ~Обязательное поле~
- **project_name**: название проекта. Так будет назван модуль Python, поэтому выбирайте валидное имя.
- **user_name**: Пользователь в /home которого располагается проект с его окружением. ~Обязательное поле~
- **admin_name** и **admin_email**: контакты администратора.
- **admin_passw**: пароль администратора для входа в django.
- **version**: версия с которой начинать проект.
- **production_host**: адрес боевого сервера.
- **site_name**: адрес сайта. Используется в ALLOWED_HOSTS. ~Обязательное поле~
- **local_db_name**, **local_db_user**, **local_db_password**: реквизиты локальной БД для разработки.
- **secret_key**: Секретный ключ Django.

Незаполненные поля будут использовать значения по умолчанию указанные в квадратных скобках.
Например: project_name [newproject]:  (По умолчанию примет значение `newproject`)

При заполнении ~обязательных полей~ проект будет запускаться командой:

.. code-block:: shell

    ./manage.py runserver 0.0.0.0:8000


По адресу `http://localhost:8000` будет доступна главная страница проекта

По адресу `http://localhost:8000/api` точка входа в REST API проекта
