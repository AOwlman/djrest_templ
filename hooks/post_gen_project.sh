#!/bin/bash
source ../env/bin/activate

echo 'Installing pip packages...'
pip install -r requirements.txt
echo 'Installing pip packages complete.'
./manage.py migrate
./manage.py add_superuser
rm ./apps/users/management/commands/add_superuser.py
echo 'DB migrate complete.'
echo '----------------------------'
echo 'dj_templ install complete.'
