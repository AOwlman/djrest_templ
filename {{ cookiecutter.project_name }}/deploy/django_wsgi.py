import os, sys

# Python/Django location
sys.path.insert(0, '/home/{{ cookiecutter.user_name }}/{{ cookiecutter.env_name }}/env')
sys.path.insert(0, '/home/{{ cookiecutter.user_name }}/{{ cookiecutter.env_name }}/env/lib/python3.6/site-packages')

# Project Location
sys.path.insert(0, '/home/{{ cookiecutter.user_name }}/{{ cookiecutter.env_name }}/')
sys.path.insert(0, '/home/{{ cookiecutter.user_name }}/{{ cookiecutter.env_name }}/{{ cookiecutter.project_name }}')

# Project configuration file


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{{ cookiecutter.project_name }}.settings.local")

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
