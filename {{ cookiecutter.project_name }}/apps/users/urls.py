from django.urls import path

from .views import hpi


urlpatterns = [
    path('',
         hpi.UsersIndexView.as_view(),
         name="users-index"),
]
