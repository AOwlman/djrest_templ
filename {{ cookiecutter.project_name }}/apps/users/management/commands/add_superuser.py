from django.core.management.base import BaseCommand

from django.contrib.auth import get_user_model

User = get_user_model()

class Command(BaseCommand):
    help = "Auto create superuser command."

    def handle(self, *args, **options):
        User.objects.create_superuser('{{ cookiecutter.admin_name }}',
                                      '{{ cookiecutter.admin_email }}',
                                      '{{ cookiecutter.admin_passw }}')
        print('User {{ cookiecutter.admin_name }} added successfully!')
