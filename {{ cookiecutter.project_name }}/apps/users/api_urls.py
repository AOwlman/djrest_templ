# API urls
from django.urls import path

from apps.users.views import api


routeList = (
    ('', api.UserViewSet),
)
