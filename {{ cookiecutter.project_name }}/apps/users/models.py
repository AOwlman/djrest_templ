import datetime
from datetime import date

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.html import format_html


class UserProfile(AbstractUser):
    '''
    User profile model
    '''
    birth_date = models.DateField('Дата рождения', blank=True, null=True)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'Профиль пользователя'
        verbose_name_plural = 'Профили пользователей'
