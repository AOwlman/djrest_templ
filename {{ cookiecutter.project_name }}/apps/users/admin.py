from django.contrib import admin

from apps.users import models

@admin.register(models.UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('username',
                    'email',
                    'date_joined',
                    'last_login',
                    'is_active',
                    'is_staff',
                    'is_superuser',
                    'id')
    list_filter = ('is_active','is_staff','is_superuser')
    date_hierarchy = 'last_login'
    search_fields = ['id', 'username', 'email']
    readonly_fields = ('id',)
