# Application Programming Interface
from django.contrib.auth import get_user_model

from rest_framework import viewsets

from {{ cookiecutter.project_name }} import utils
from apps.users import serializers


User = get_user_model()

class UserViewSet(viewsets.ModelViewSet, utils.ApiBasicAuth):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
