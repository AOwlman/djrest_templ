# Humans Programming Interface
from django.views.generic.base import TemplateView


class UsersIndexView(TemplateView):
    template_name = "users-index.html"
