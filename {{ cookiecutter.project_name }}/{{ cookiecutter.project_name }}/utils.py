# Common utilities for the project
from django.conf import settings

from rest_framework.authentication import (SessionAuthentication,
                                           TokenAuthentication,
                                           BasicAuthentication)
from rest_framework.permissions import IsAuthenticated


class ApiBasicAuth:
    '''
    Base authentication class for project
    '''
    if not settings.DEBUG:
        authentication_classes = (BasicAuthentication,
                                  TokenAuthentication,
                                  SessionAuthentication)
        permission_classes = (IsAuthenticated,)

