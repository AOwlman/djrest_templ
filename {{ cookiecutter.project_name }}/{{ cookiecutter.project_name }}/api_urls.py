from rest_framework import routers

from apps.users import api_urls as users_urls

routeLists = [
    users_urls.routeList,
]
router = routers.DefaultRouter()

for routeList in routeLists:
    for route in routeList:
        router.register(route[0], route[1])

urlpatterns = router.urls
