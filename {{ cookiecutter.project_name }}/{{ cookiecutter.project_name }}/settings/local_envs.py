LOCAL_DEBUG = True
LOCAL_SECRET_KEY = '{{ cookiecutter.secret_key }}'
LOCAL_ALLOWED_HOSTS = ['{{ cookiecutter.site_name }}',
                       '127.0.0.1',
                       'localhost']

ADMINS = [
    ('{{ cookiecutter.admin_name }}', '{{ cookiecutter.admin_email }}'),
]

DATABASE_NAME = '{{ cookiecutter.local_db_name }}'
DATABASE_NAME_USER = '{{ cookiecutter.user_name }}'
DATABASE_USER_PSW = '{{ cookiecutter.local_db_password }}'
