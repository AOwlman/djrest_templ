from .local_envs import *
from .base import *

# Раскоментировать после создания базы в PostgreSQL
# DATABASES = {
    # 'default': {
        # 'ENGINE': 'django.db.backends.postgresql_psycopg2',
        # 'NAME': DATABASE_NAME,
        # 'USER': DATABASE_NAME_USER,
        # 'PASSWORD': DATABASE_USER_PSW,
        # 'HOST': 'localhost',
        # 'PORT': '5432',
    # }
# }

# Закомментировать или удалить после создания базы в PostgreSQL
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
