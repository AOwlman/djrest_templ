import os

from .local_envs import *

SETTINGS_PATH = os.path.dirname(os.path.abspath(__file__))

BASE_DIR = os.path.dirname(os.path.dirname(SETTINGS_PATH))

SECRET_KEY = LOCAL_SECRET_KEY

DEBUG = LOCAL_DEBUG

ALLOWED_HOSTS = LOCAL_ALLOWED_HOSTS

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Other
    'corsheaders',
    'django_extensions',
    'django_filters',
    'rest_framework',
    # Our
    '{{ cookiecutter.project_name }}',
    'apps.users',
]

AUTH_USER_MODEL = 'users.UserProfile'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
]


ROOT_URLCONF = '{{ cookiecutter.project_name }}.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates',
                 '%s/templates/' % BASE_DIR],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                '{{ cookiecutter.project_name }}.context_processors.prjset',
            ],
        },
    },
]

WSGI_APPLICATION = '{{ cookiecutter.project_name }}.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# LANGUAGES
LANGUAGE_CODE = 'ru-RU'

LANGUAGES = (
    ('ru', 'Русский'),
    ('en', 'English'),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True

DATETIME_FORMAT = 'd.m.Y H:i:s'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

STATIC_ROOT = '%s/public/static/' % BASE_DIR
MEDIA_ROOT = '%s/public/media/' % BASE_DIR


## Настройки в зависимости от режима DEBUG
if DEBUG:
    # DjangoREST permissions
    PERMISSION_CLASS = (
        'rest_framework.permissions.AllowAny',
    )

else:
    # DjangoREST permissions
    PERMISSION_CLASS = (
        'rest_framework.permissions.IsAuthenticated',
    )

## Настройки сторонних приложений
# django-rest
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        #'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': PERMISSION_CLASS,
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
}



# CORS-HEADERS
CORS_ORIGIN_ALLOW_ALL = True
# CORS_ORIGIN_WHITELIST = SERVER_CORS_WHITELIST
